// this is HomeController
module.exports = {
    // hiển thị trang chủ
    show_home_page: function( req, res) {
        res.render('index') // gọi đến file index.ejs trong views
    },

    // hiển thị trang demo socket
    show_socket_page: (req, res) => {
        res.render('socket_io') // gọi đến file socket_io.ejs trong views
    }
}