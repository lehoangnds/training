var userSchema = require('../models/Users.js'); // chèn file user bên thư mục models
var Model = require('../models/Model'); // chèn file model
module.exports = {
    insert_user: function(req, res){
        console.log(req.body);

        // luu du lieu vao database
        /* var User = new userSchema();
        	User.fullname = req.body.fullname;
        	User.username = req.body.username;
        	User.password = req.body.password;
        User.save(function(err, data){
			if(err){
				res.json({status: 400, msg: "Khong them duoc du lieu"});
			}else{
				res.json({status: 200, data: data, msg: "Them du lieu thanh cong"});
			}
		}) */

		// them du lieu vao mysql
		var fullname = req.body.fullname,
        	username = req.body.username,
        	password = req.body.password; 
		Model.insert(fullname, username, password, cb => {
			console.log(cb);
			if(cb.status == 400){
				res.json({status: 400, msg: "Khong the them du lieu"})
			}else{
				res.json({status: 200, msg: 'Them du lieu thanh cong', data: cb.data})
			}
		})

	},
	
	// hiển thị trang lấy danh sách user
	show_user_page: (req, res) => {
		res.render('users')
	},

	// lấy danh sách user
	get_users: (req, res) => {
		// sử dụng userSchema 
		userSchema.find( (err, data) => {
			if(err){
				console.log(err);
				res.json({users: []})
			}else{
				res.json({users: data})
			}
			
		});
	}
}