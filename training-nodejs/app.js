var express 	= require("express"), // npm install express
    app 		= express(),
    bodyParser= require('body-parser') // npm install body-parser
    path 		= require('path'), // npm install path
    server 	    = require('http').Server(app),
    port 		=  3000,
    

app.use(express.static('public')) // set thư mục mặc định
app.use(bodyParser.urlencoded({extended : true})); // sử dụng body
app.use(bodyParser.json()); // sử dụng body

const databaseConfig = require('./config/database'); // Chèn database
databaseConfig(); // khởi tạo database

// demo socket.io
var io = require('socket.io')(server); // npm install socket.io
io.on('connection', socket => { // sự kiện kết nối socket
    console.log("socket connected: " + socket.id);
    
    // nhận data từ client 
    socket.on('hello', data => {
        console.log(data)
        // gửi message tới all client
        socket.broadcast.emit('say to all', {name: data.name, msg: 'Gửi mọi người'});

        // gửi message cho chính mình
        socket.emit('myself', {msg: 'Gửi chính mình'})
    })

    socket.on('disconnect', () => { // socket ngắt kết nối
    	console.log('Socket disconnect: ', socket.id);
    });

});

// sử dụng onesignal
var client = require('./config/onesignal'); // file config
client.createNotification({
    contents: {
        contents: "Phần nội dugn hiển thị",
        headings: "Phần header "
    },
    specific: {
        include_player_ids: ['457664a8-9b11-49d9-97a2-7c0d9161f830'] // player_id được cung cấp khi đăng ký dịch vụ onesignal
    },
    attachments: {
        data: { 
            // data truyền sang
        }
    }
}).then(success => {
    console.log("Gửi onesignal thành công");
}).catch(e => {
    console.log('Gửi onesignal thất bại')
    console.log(e);
});

app.set('view engine' , 'ejs'); // sử dụng view engine EJS
app.set('views', path.join(__dirname +  '/views')); // cài đặt thư mục view mặc định

app.use('/', require('./routes/home'));	// sử dụng route

server.listen(port, function(err){
    if(err){
        console.log(err);
    }else{
        console.log('Listen port on '+  port);
    }
});
