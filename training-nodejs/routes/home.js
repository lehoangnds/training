var express     = require('express'),
router      = express.Router(); // sử dụng router của express

/*
* chèn controller
*/
var  Home = require('../controllers/HomeController.js');
var  User  = require('../controllers/UserController.js');

/*
* gọi đến function show_home_page() trong controller HomeController.js
*/
router.get('/', Home.show_home_page) // hiển thị trang chủ
      .post('/', User.insert_user) // insert user
      .get('/socket', Home.show_socket_page) // hiển thị trang demo socket
      .get('/users', User.show_user_page) // Hiển thị trang láy ds user
      .get('/list_users', User.get_users) // lấy danh sách user


module.exports = router;