var mysql = require('mysql');

var conn =  mysql.createConnection({
	  host: "localhost",
	  user: "root",
	  password: "123456",
	  database: 'training_nodejs'
	});
conn.connect( (err) => {
	if (err) throw err;
	console.log("Mysql Connected!");
});
module.exports = conn;