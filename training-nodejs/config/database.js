var mongoose = require('mongoose'); // npm install mongoose

module.exports = function(){
	var dbName = "training-nodejs"; // tên database
	mongoose.Promise = global.Promise;
	mongoose.connect('mongodb://localhost/' + dbName);
	mongoose.connection
			.once('open', function(){
				console.log("* Da ket noi MongoDb: " + dbName);
			})
			.on('error', function(err){
				console.error("* Loi...Khong the ket noi den database.");
			});
}
