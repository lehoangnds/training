import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'; // npm install react-router-dom

const Child = ({match}) => {
    return(<h3>Menu: { match.params.id}</h3>)
}
class Menu extends Component {
    render(){
        return (
            <Router>
                <div>
                    <h2>Menu</h2>
                    <ul>
                        <li><Link to="/item1">Item 1</Link></li>
                        <li><Link to="/item2">Item 2</Link></li>
                        <li><Link to="/item3">Item 3</Link></li>
                        <li><Link to="/item4">Item 4</Link></li>
                    </ul>
                    <Route path="/:id" component={Child}/> {/* Lay ra dia chi url */}
                </div>
            </Router>
        )
    }
}
export default Menu
