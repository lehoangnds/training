import React, { Component } from 'react';

class State extends Component{
    constructor(props){
        super(props);
        this.state = {
            header: 'Header from state'
        }
    }

    // change state
    onClick(e) {
        this.setState({ header:  e.target.textContent + ' after click'})
    }
    render() {
        return(<p onClick={this.onClick.bind(this)}>{this.state.header}</p>)
    }
}
export default State;