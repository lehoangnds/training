import React, {Component} from 'react';

class Demo_props extends Component {
    render(){
        return(
            <div>
                <h1>{this.props.username}</h1>
            </div>
        )
    }
}

export default Demo_props