import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// chèn prop component
import Demo_props from './components/props'
ReactDOM.render(<App />, document.getElementById('root'));
// root là id của tagname bên file public/index.html

ReactDOM.render(<Demo_props username = 'hoangsnow' />, document.getElementById('props'));
// props là id của tagname bên file public/index.html

registerServiceWorker();
