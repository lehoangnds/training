import React, { Component } from 'react';
import './App.css';

//  chèn compnents
import State from './components/state';
import Header from './components/header';
import Content from './components/content';
import Menu from './components/menu';
class App extends Component {
  render() {
    return (
      <div>
        <Menu />
        <Header /> {/*component được import */}
        <Content />
        <State />
      </div>
    );
  }
}
export default App;
