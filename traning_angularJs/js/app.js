var app = angular.module('my_module', ['ngRoute']); // tạo module

app.config( function($routeProvider){ // config router
    $routeProvider
    .when('/hanoi', {
        template: '<h1>Hello Ha Noi</h1>'
    }).when('/namdinh', {
        template : "<h1>Hello Nam Dinh</h1>"
    })
})

app.controller('my_controller',[ '$scope','$http',function($scope, $http) { // tạo controller
    // $scope: Là đối tượng giao tiếp giữa cntroller và view
    // $htttp: Hỗ trợ giao tiếp với server
    $scope.hello = 'Traning angularJS';

    // su dung $http gui 1 request len server theo url
    $http.get('http://rest-service.guides.spring.io/greeting').then(function(response) {
        console.log(response.data);
        $scope.greeting = response.data;
    });
    
}]);