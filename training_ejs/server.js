var express 	= require("express"), // npm install express
    app 		= express(),
    path 		= require('path'), // npm install path
    server 	    = require('http').Server(app),
    ejs         = require('ejs'), // npm install ejs
    port 		=  6969;

app.set('view engine' , 'ejs'); // sử dụng view engine EJS
app.set('views', path.join(__dirname +  '/views')); // cài đặt thư mục view mặc định

app.get('/', (req, res) => {
    var data = [
        {
            id: 1,
            name: 'lê',
            email: 'le@gmail.com'
        },
        {
            id: 2,
            name: 'Hoàng',
            email: 'hoang@gmail.com'
        }

    ]
    res.render('index', {data: data}) // truyền data qua view
})

server.listen(port, function(err){
    if(err){
        console.log(err);
    }else{
        console.log('Listen port on '+  port);
    }
});
