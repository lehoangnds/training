
module.exports.routes = {
  // method get
  'get /api/users': 'User.get_users',

  // method post
  'post /api/users': 'User.post_user',

  // method put
  'put /api/users': 'User.put_user',

  // method delete
  'delete /api/users': 'User.delete_user'
};
