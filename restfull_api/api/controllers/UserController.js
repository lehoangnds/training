/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var fs = require('fs');
module.exports = {
    // tạo mới user
	post_user: (req, res) => {
        var full_name = req.body.full_name,
            username = req.body.username,
            password = req.body.password,
            avatar = req.file('avatar');
        // upload file
        avatar.upload({dirname: sails.config.appPath + '/assets/images'}, function (err, uploadedFiles) {
            maxBytes: 10000000;
            var filename = uploadedFiles[0].fd.substring(uploadedFiles[0].fd.lastIndexOf('/') + 1);
            var uploadLocation = process.cwd() + '/assets/images/' + filename;
            var tempLocation = process.cwd() + '/.tmp/public/images/' + filename;
            // chuyen file tam .tmp sang assest
            fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
            User.create({full_name: full_name,username: username, password: password, avatar: filename }, (err, user) => {
                if(err){
                    res.json({status: 400})
                    console.log(err)
                }else{
                    var url = "http://localhost:1337/images";
                    user.avatar = url + user.avatar;
                    res.json({status: 200, userInfo: user})
                }
            })
                
            
        });
    },

    // method get
    get_users: (req, res) => {
        // get all users
        User.find( (err, users) => {
            if(err){
                res.json({status: 400})
            }else{
                res.json({status: 200, users: users})
            }
        })
    },

    // method PUT
    put_user: (req, res) => {
        // Update user
        var user_id = req.body.user_id;
        var info_update = {
            full_name: req.body.full_name,
            username: req.body.username
        }
        User.update({id: user_id}, info_update, (err, updated) => {
            if(err){
                res.json({status: 400})
            }else{
                res.json({status: 200, user: updated[0]})
            }
        })
    },

    // method DELETE
    delete_user: (req, res) => {
        var user_id = req.body.user_id;
        // xoa user
        User.destroy({id: user_id}, err => {
            if(err){
                res.json({status: 400})
            }else{
                res.json({status: 200, msg: "Xoa user thanh cong"})
            }
        } )
    }
};

