module.exports = {
  attributes: {
    full_name: {
      type: 'string'
    },

    username: {
      type: 'string'
    },

    password: {
      type: 'string'
    },

    avatar: {
      type: 'string'
    }
  }
};
