class AddStatusToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :status, :string
    # => :articles là tên table muốn thay đổi
    # :status Là tên column muốn thêm vào table articles
    # :string là kiểu dữ liệu
  end
end
