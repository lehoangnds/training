Rails.application.routes.draw do
  get 'welcome/index' 

  # Đường dẫn mặc định localhost:3000 
  # sẽ gọi đến function index trong welcome controller
  root 'welcome#index'

  # rails cung cấp method resources
  resources :articles do 
    resources :comments
  end

  # có thể dùng router tự tạo thay vì method resources của rails
  get 'demo' => 'welcome#demo_render' # gọi đền function demo_render 
                                      # trong welcome_controller

end

