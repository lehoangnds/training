class Article < ApplicationRecord
    # Một bài viết có nhiều comment
    has_many :comments, dependent: :destroy
    # sử dụng dependent destroy 
    # Khi xóa 1 bài viết sẽ tự động xóa các comments của nó

    validates :title, length: { minimum: 5, :message => "phải nhiều hơn 5 ký tự" }
    validates_presence_of  :title,:message => "không được để trống"               
    # Validate title, bắt buộc phải có, và độ dài chuỗi nhỏ nhất = 5
end
