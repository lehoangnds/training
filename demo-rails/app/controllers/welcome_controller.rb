class WelcomeController < ApplicationController
  def index
  end

  # demo render
  def demo_render
    render :layout => 'welcome'
  end
end
