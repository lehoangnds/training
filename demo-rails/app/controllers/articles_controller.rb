class ArticlesController < ApplicationController
    layout 'article'
    # function hiển thị form post bài viết
    def new
        @article = Article.new
    end

    # function thêm mới bài viết
    def create
        @article = Article.new(article_params)
        if @article.save
            redirect_to @article
        else
            render 'new'
        end
    end
    
    # hiển thị chi tiết bài viết theo id
    def show
        @article = Article.find(params[:id])
    end

    # Hiển thị danh sách bài viết
    def index
        @articles = Article.all # lấy tất cả bài viết đang có
    end

    # hiển thị trang sửa bài viết
    def edit
        @article = Article.find(params[:id])
    end
    # update thông tin bài viết
    def update 
        @article = Article.find(params[:id])
        
        # Kiểm tra nếu update thành công thì chuyển qua trang chi tiết
        if @article.update_attributes(article_params)
            redirect_to @article
        else # nếu không thành công thì quay lại trang edit
            render 'edit'
        end
    end 

    # Xóa bài viết
    def destroy
        @article = Article.find(params[:id])
        @article.destroy
    
        redirect_to articles_path
    end

    private
        def article_params
            # chỉ nhận param title và text
            params.require(:article).permit(:title, :text)
        end
end
