var app = new Vue({
    el: '.app',
    data: {
        message: 'hello vue'
    }
})

var app1 = new Vue({
    el: '.app1',
    data: {
        message: 'title is '+ new Date().toLocaleString()
    }
})

var app2 = new Vue({
    el: '.app2',
    data: {
        list_users: [
            {
                id: 1,
                name: 'le van hoang',
                email: 'hoangsnow@gmail.com',
                status: 0
            },
            {
                id: 2,
                name: 'le thi hoang',
                email: 'hoangsnow1@gmail.com',
                status: 1
            },
            {
                id: 3,
                name: 'le thai hoang',
                email: 'hoangsnow2@gmail.com',
                status: 1
            }
        ],
        totals: 0
    },
    methods: {
        addUser: function() {
            this.totals += 1
        }
    }
})

var app3 = new Vue({
    el: '.login',
    data: {
        username: '',
        password: ''
    },
    methods: {
        signup: function(){
            let user = {
                username: this.username,
                password: this.password
            }
            console.log(user)
        }
    }
})